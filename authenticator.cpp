/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  David Edmundson <d.edmundson@lboro.ac.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "authenticator.h"

#include <QDebug>

#define nullptr 0

#include <security/pam_appl.h>
#include <security/pam_modules.h>

    class PamService {
    public:
        PamService(const char *service, const QString &user, const QString &password);
        ~PamService();

        struct pam_conv m_converse;
        pam_handle_t *handle;
        QString user;;
        QString password;
    };

    int converse(int n, const struct pam_message **msg, struct pam_response **resp, void *data) {
        struct pam_response *aresp;

        // check size of the message buffer
        if ((n <= 0) || (n > PAM_MAX_NUM_MSG))
            return PAM_CONV_ERR;

        // create response buffer
        if ((aresp = (struct pam_response *) calloc(n, sizeof(struct pam_response))) == nullptr)
            return PAM_BUF_ERR;

        // respond to the messages
        bool failed = false;
        for (int i = 0; i < n; ++i) {
            qDebug() << msg[i]->msg_style << msg[i]->msg;
            aresp[i].resp_retcode = 0;
            aresp[i].resp = nullptr;
            switch (msg[i]->msg_style) {

                case PAM_PROMPT_ECHO_OFF: {
                    qDebug() << "set password";
                    PamService *c = static_cast<PamService *>(data);
                    // set password
                    aresp[i].resp = strdup(qPrintable(c->password));
                    if (aresp[i].resp == nullptr)
                        failed = true;
                    // clear password
                    c->password = "";
                }
                    break;
                case PAM_PROMPT_ECHO_ON: {
                    qDebug() << "set user";
                    PamService *c = static_cast<PamService *>(data);
                    // set user
                    aresp[i].resp = strdup(qPrintable(c->user));
                    if (aresp[i].resp == nullptr)
                        failed = true;
                    // clear user
                    c->user = "";
                }
                    break;
                case PAM_ERROR_MSG:
                case PAM_TEXT_INFO:
                    break;
                default:
                    failed = true;
            }
        }

        if (failed) {
            for (int i = 0; i < n; ++i) {
                if (aresp[i].resp != nullptr) {
                    memset(aresp[i].resp, 0, strlen(aresp[i].resp));
                    free(aresp[i].resp);
                }
            }
            memset(aresp, 0, n * sizeof(struct pam_response));
            free(aresp);
            *resp = nullptr;
            return PAM_CONV_ERR;
        }

        *resp = aresp;
        return PAM_SUCCESS;
    }

    PamService::PamService(const char *service, const QString &user, const QString &password) :
        user(user),
        password(password),
        handle(0)
    {
        // create context
        m_converse = { &converse, this };

        // start service
        pam_start(service, nullptr, &m_converse, &handle);
    }

    PamService::~PamService() {
        // stop service
        pam_end(handle, PAM_SUCCESS);
    }


Authenticator::Authenticator(QObject* parent):
    QObject(parent)
{
    qDebug() << "logging in " << start();
    exit(0);
}

Authenticator::~Authenticator()
{
}

bool Authenticator::start()
{
    QString user("test");
    QString password("test");

    PamService m_pam("login", user, password);
    // authenticate the applicant
    return pam_authenticate(m_pam.handle, 0) == PAM_SUCCESS;
}


#include "authenticator.moc"
