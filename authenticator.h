/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  David Edmundson <d.edmundson@lboro.ac.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H

#include <QObject>

class Authenticator : public QObject
{
    Q_OBJECT

//     Q_PROPERTY activePrompt()
//     Q_PROPERTY state();

enum State {
    Inactive,
    WaitingForAuthentication,
    Failed,
    Success
};
enum Prompt {
    Password,
    FingerPrint
};

public:
    Authenticator(QObject *parent=0);
    virtual ~Authenticator();

    bool start();

//     void setServiceName(const QString &serviceName);
//     State currentState();
//     Prompt activePrompt();
//     QList<Prompt> passivePrompts();

Q_SIGNALS:
//     void stateChanged();
//     void activePromptChanged();
//     void passivePromptsChanged();
//     void authFailed();

public Q_SLOTS:
//     void startAuthentication(const QString &username);
//     void provideResponse(const QByteArray &response);
//     void abort();

private:
};

#endif // AUTHENTICATOR_H
